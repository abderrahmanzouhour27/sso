import { useState, useEffect } from "react";
import { supabase } from "./client";
import Login from "./component/Login";
import Login2 from "./component/Login2";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
function App() {
  const [user, setUser] = useState([]);
  useEffect(() => {
    checkUser();
    window.addEventListener("hashchange", function () {
      checkUser();
    });
  }, []);
  async function checkUser() {
    const user: any = supabase.auth.user();
    setUser(user);
  }
  async function signInWithGithub() {
    await supabase.auth.signIn({
      provider: "github",
    });
  }
  async function signOut() {
    await supabase.auth.signOut();
    setUser([]);
    console.log("user:", user);
  }
  if (user) {
    let use: any = user;
    console.log(use.email);
    return (
      <div className="App">
        <h1> Hello {use.email}</h1>
        <button onClick={signOut}> Sign out</button>
      </div>
    );
  }
  return (
    <div className="App">
      <div>
        {/* <Login /> */}
        <Login2 />
      </div>
      <h1>Hello, please sign in!</h1>
      <button
        className="login__submit  justify-content-center"
        onClick={signInWithGithub}
      >
        Sign In
      </button>
    </div>
  );
}

export default App;
