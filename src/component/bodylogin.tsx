import React, { useState } from "react";
// import React, { useRef } from "react";
// import ReactDOM from "react-dom";
// import useForm from "react-hook-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEye,
  faEyeSlash,
  faTimes,
  faCheck,
} from "@fortawesome/free-solid-svg-icons";
// import "./bodylogin.css";
import "./login.css";
function Bodylogin() {
  // const { register, errors, handleSubmit, watch } = useForm({});
  // const password = useRef({});
  // password.current = watch("password", "");
  // const onSubmit = async (data: any) => {
  //   alert(JSON.stringify(data));
  // };

  // return (
  //   <form onSubmit={(e) => e.preventDefault()}>
  //     <label>Password</label>
  //     <input
  //       name="password"
  //       type="password"
  //       ref={register({
  //         required: "You must specify a password",
  //         minLength: {
  //           value: 8,
  //           message: "Password must have at least 8 characters",
  //         },
  //         pattern: {
  //           value:
  //             /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
  //           message:
  //             "password must contain lowercase and uppercase and special caracter", // JS only: <p>error message</p> TS only support string
  //         },
  //       })}
  //     />
  //     {errors.password && <p>{errors.password.message}</p>}

  //     <label>Repeat password</label>
  //     <input
  //       name="password_repeat"
  //       type="password"
  //       ref={register({
  //         validate: (value: any) =>
  //           value === password.current || "The passwords do not match",
  //       })}
  //     />
  //     {errors.password_repeat && <p>{errors.password_repeat.message}</p>}

  //     <input type="submit" onClick={handleSubmit(onSubmit)} />
  //   </form>
  // );
  const [show, setShow] = useState(false);
  //if valid
  const valid = (item: any, v_icon: any, inv_icon: any) => {
    let text: any = document.querySelector(`#${item}`);
    text.style.opacity = "1";
    let valid_icon: any = document.querySelector(`#${item} .${v_icon}`);
    valid_icon.style.opacity = "1";
    let invalid_icon: any = document.querySelector(`#${item} .${inv_icon}`);
    invalid_icon.style.opacity = "0";
  };
  // if invalid
  const invalid = (item: any, v_icon: any, inv_icon: any) => {
    let text: any = document.querySelector(`#${item}`);
    text.style.opacity = "5";
    let valid_icon: any = document.querySelector(`#${item} .${v_icon}`);
    valid_icon.style.opacity = "0";
    let invalid_icon: any = document.querySelector(`#${item} .${inv_icon}`);
    invalid_icon.style.opacity = "1";
  };
  // handle input
  const handleInputChange = (e: any) => {
    const password = e.target.value;
    if (password.match(/[A-Z]/) != null) {
      valid("capital", "fa-check", "fa-times");
    } else {
      invalid("capital", "fa-check", "fa-times");
    }
    if (password.match(/[0-9]/) != null) {
      valid("num", "fa-check", "fa-times");
    } else {
      invalid("num", "fa-check", "fa-times");
    }
    if (password.match(/[!@#$%^&*]/) != null) {
      valid("char", "fa-check", "fa-times");
    } else {
      invalid("char", "fa-check", "fa-times");
    }
    if (password.length > 7) {
      valid("more8", "fa-check", "fa-times");
    } else {
      invalid("more8", "fa-check", "fa-times");
    }
  };

  const handleShowhide = () => {
    setShow(!show);
  };
  return (
    <div>
      <form>
        <div className="mb-3">
          <div className="form-label">Email Address</div>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            placeholder="Enter your Email"
          />
          <div id="emailHelp" className="form-text">
            We'll never share your email with anyone else.
          </div>
        </div>
        <div className="mb-3">
          <div className="form-label">Password</div>
          <input
            type={show ? "text" : "password"}
            className="form-control"
            id="exampleInputPassword1"
            placeholder="Enter your Password"
            onChange={handleInputChange}
          />
          {show ? (
            <FontAwesomeIcon
              onClick={handleShowhide}
              icon={faEye}
              id="show_hide"
            />
          ) : (
            <FontAwesomeIcon
              onClick={handleShowhide}
              icon={faEyeSlash}
              id="show_hide"
            />
          )}
          <p id="capital">
            <FontAwesomeIcon className="fa-times icon" icon={faTimes} />
            <FontAwesomeIcon className="fa-check icon" icon={faCheck} />
            <span> Capital Letters </span>
          </p>
          <p id="char">
            <FontAwesomeIcon className="fa-times icon" icon={faTimes} />
            <FontAwesomeIcon className="fa-check icon" icon={faCheck} />
            <span> Special Characters </span>
          </p>
          <p id="num">
            <FontAwesomeIcon className="fa-times icon" icon={faTimes} />
            <FontAwesomeIcon className="fa-check icon" icon={faCheck} />
            <span> Use Numbers </span>
          </p>
          <p id="more8">
            <FontAwesomeIcon className="fa-times icon" icon={faTimes} />
            <FontAwesomeIcon className="fa-check icon" icon={faCheck} />
            <span> 8+ Characters </span>
          </p>
        </div>
        <button type="submit" className="btn btn-primary">
          Login
        </button>
      </form>
    </div>
  );
}

export default Bodylogin;
