import React, { useState } from "react";
import { Button } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import BodyLogin from "./bodylogin";

import "./style.css";

function Login2() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Button
        className="  login__submit justify-content-center"
        variant="primary"
        onClick={handleShow}
      >
        Launch demo modal
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="box-form">
            <div className="left">
              <div className="overlay">
                <h1>Hello World.</h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Curabitur et est sed felis aliquet sollicitudin
                </p>
              </div>
            </div>
            <div className="right">
              <h5 className="my-5">Login</h5>
              <BodyLogin />
            </div>
          </div>
        </Modal.Body>
          </Modal>
    </>
  );
}

export default Login2;
